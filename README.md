# Hockey Manager Application

The Hockey Manager Application is a RESTful API for managing hockey players' data. It allows you to add players of different positions (forwards, defenders, and goalies) and retrieve information about the youngest player(s) in the system.

## Technologies Used

- Java 17
- Spring Boot
- H2 Database

## Prerequisites

- Java 8 or higher
- Maven

## Installation

1. Clone the repository:


    git clone <repository-url>

2. Build the application:


      cd hockey-manager

      mvn clean install

3. Starting the Application

To start the application, run the following command:

    mvn spring-boot:run

The application will be available at http://localhost:8080.

## Endpoints

### Add Forward Player
- POST - /players/forward

Adds a new forward player to the system.

##### Request Body

{
"name": "John Doe",
"age": 25,
"goals": 11
}

 - name (string, required): The name of the player.
 - age (integer, required): The age of the player.
 - goals (integer, required): The number of goals scored by the player.

### Add Defender Player
- POST - /players/defender

Adds a new forward player to the system.

##### Request Body

{
"name": "Jane Smith",
"age": 23,
"hits": 30
}

- name (string, required): The name of the player.
- age (integer, required): The age of the player.
- hits (integer, required): The number of hits made by the player.

### Add Goalie Player
- POST - /players/goalie

Adds a new forward player to the system.

##### Request Body

{
"name": "Peter Johnson",
"age": 18,
"wins": 10
}

- name (string, required): The name of the player.
- age (integer, required): The age of the player.
- wins (integer, required): The number of wins achieved by the player.

### Get Youngest Player(s)
- GET /players/youngest

Retrieves the youngest player(s) in the system. If there is more than one player with the same youngest age, all of them 
will be returned in the response.

##### Response Body

[
{
"name": "John Doe",
"age": 25
},
{
"name": "Jane Smith",
"age": 25
}
]

- name (string, required): The name of the player.
- age (integer, required): The age of the player.

##### Please note that all fields (name, age, goals, hits, wins) are required when adding a player, and the youngest endpoint may return multiple players if they have the same youngest age.



