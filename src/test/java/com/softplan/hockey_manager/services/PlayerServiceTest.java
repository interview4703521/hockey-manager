package com.softplan.hockey_manager.services;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.*;
import static org.mockito.Mockito.*;

import java.util.Arrays;
import java.util.List;

import com.softplan.hockey_manager.dto.PlayerDto;
import com.softplan.hockey_manager.entities.Defender;
import com.softplan.hockey_manager.entities.Forward;
import com.softplan.hockey_manager.entities.Goalie;
import com.softplan.hockey_manager.entities.Player;
import com.softplan.hockey_manager.repositories.PlayerRepository;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

@ExtendWith(MockitoExtension.class)
class PlayerServiceTest {

    @Mock
    private PlayerRepository playerRepository;

    @InjectMocks
    private PlayerService playerService;

    @Test
    public void testAddNewForward() {
        Forward forward = new Forward("John Doe", 25, 10);

        playerService.addNewForward(forward);

        verify(playerRepository, times(1)).save(forward);
    }

    @Test
    public void testAddNewDefender() {
        Defender defender = new Defender("Jane Smith", 28, 50);

        playerService.addNewDefender(defender);

        verify(playerRepository, times(1)).save(defender);
    }

    @Test
    public void testAddNewGoalie() {
        Goalie goalie = new Goalie("Alex Johnson", 30, 15);

        playerService.addNewGoalie(goalie);

        verify(playerRepository, times(1)).save(goalie);
    }

    @Test
    public void testPrintNameAndAgeOfTheYoungestPlayer() {
        Player player1 = new Player("John Doe", 25);
        Player  player2 = new Player("Jane Smith", 25);

        List<Player> youngestPlayers = Arrays.asList(player1, player2);

        when(playerRepository.findAllByOrderByAgeAsc()).thenReturn(youngestPlayers);

        List<PlayerDto> result = playerService.printNameAndAgeOfTheYoungestPlayer();

        assertThat(result.size(), is(youngestPlayers.size()));
        assertThat(result.get(0).getName(), is(player1.getName()));
        assertThat(result.get(0).getAge(), is(player1.getAge()));
        assertThat(result.get(1).getName(), is(player2.getName()));
        assertThat(result.get(1).getAge(), is(player2.getAge()));

        verify(playerRepository, times(1)).findAllByOrderByAgeAsc();
    }
}
