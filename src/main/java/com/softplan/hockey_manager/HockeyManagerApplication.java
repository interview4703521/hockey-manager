package com.softplan.hockey_manager;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class HockeyManagerApplication {

    public static void main(String[] args) {
        SpringApplication.run(HockeyManagerApplication.class, args);
    }

}
