package com.softplan.hockey_manager.controllers;

import com.softplan.hockey_manager.dto.PlayerDto;
import com.softplan.hockey_manager.entities.Defender;
import com.softplan.hockey_manager.entities.Forward;
import com.softplan.hockey_manager.entities.Goalie;
import com.softplan.hockey_manager.services.PlayerService;
import jakarta.validation.Valid;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@RequestMapping("/players")
@RequiredArgsConstructor
public class PlayerController {

    private final PlayerService playerService;

    @PostMapping("/forward")
    public void addForward(@RequestBody @Valid Forward forward) {
        playerService.addNewForward(forward);
    }

    @PostMapping("/defender")
    public void addDefender(@RequestBody @Valid Defender defender) {
        playerService.addNewDefender(defender);
    }

    @PostMapping("/goalie")
    public void addGoalie(@RequestBody @Valid Goalie goalie) {
        playerService.addNewGoalie(goalie);
    }

    @GetMapping("/youngest")
    public List<PlayerDto> getYoungestPlayer() {
       return playerService.printNameAndAgeOfTheYoungestPlayer();
    }
}
