package com.softplan.hockey_manager.services;

import com.softplan.hockey_manager.dto.PlayerDto;
import com.softplan.hockey_manager.entities.Defender;
import com.softplan.hockey_manager.entities.Forward;
import com.softplan.hockey_manager.entities.Goalie;
import com.softplan.hockey_manager.repositories.PlayerRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
@RequiredArgsConstructor
public class PlayerService {

    private final PlayerRepository playerRepository;

    public void addNewForward(Forward forward) {
        playerRepository.save(forward);
    }

    public void addNewDefender(Defender defender) {
        playerRepository.save(defender);
    }

    public void addNewGoalie(Goalie goalie) {
        playerRepository.save(goalie);
    }

    public List<PlayerDto> printNameAndAgeOfTheYoungestPlayer() {
        var youngestPlayer = playerRepository.findAllByOrderByAgeAsc();

        return youngestPlayer.stream()
                .map(p -> PlayerDto.builder()
                        .name(p.getName())
                        .age(p.getAge())
                        .build())
                .toList();
    }
}
