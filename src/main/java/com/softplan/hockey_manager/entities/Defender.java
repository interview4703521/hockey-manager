package com.softplan.hockey_manager.entities;

import jakarta.persistence.Entity;
import jakarta.validation.constraints.NotNull;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Entity
@Getter
@Setter
@NoArgsConstructor
public class Defender extends Player{
    @NotNull
    private Integer hits;

    public Defender(String name, Integer age, Integer hits) {
        super(name, age);
        this.hits = hits;
    }
}
