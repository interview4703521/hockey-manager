package com.softplan.hockey_manager.entities;

import jakarta.persistence.Entity;
import jakarta.validation.constraints.NotNull;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Entity
@Getter
@Setter
@NoArgsConstructor
public class Forward extends Player {
    @NotNull
    private Integer goals;

    public Forward(String name, Integer age, Integer goals) {
        super(name, age);
        this.goals = goals;
    }
}
