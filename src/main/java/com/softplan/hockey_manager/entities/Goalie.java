package com.softplan.hockey_manager.entities;

import jakarta.persistence.Entity;
import jakarta.validation.constraints.NotNull;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Entity
@Getter
@Setter
@NoArgsConstructor
public class Goalie extends Player {
    @NotNull
    private Integer wins;

    public Goalie(String name, Integer age, Integer wins) {
        super(name, age);
        this.wins = wins;
    }
}
