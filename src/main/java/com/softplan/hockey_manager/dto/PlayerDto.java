package com.softplan.hockey_manager.dto;

import lombok.Builder;
import lombok.Data;

@Data
@Builder
public class PlayerDto {

    private String name;

    private Integer age;
}
